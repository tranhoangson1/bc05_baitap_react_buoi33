import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoeShop from "./ListShoeShop";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({ cart: cloneCart });
  };

  handleDetail = (value) => {
    this.setState({ detail: value });
  };
  render() {
    return (
      <div className="container">
        <Cart cartItem={this.state.cart} />
        <ListShoeShop
          shoeArr={this.state.shoeArr}
          handleDetail={this.handleDetail}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
