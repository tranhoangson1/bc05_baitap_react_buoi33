import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoeShop extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item) => {
      return (
        <ItemShoe
          data={item}
          handleViewDetail={this.props.handleDetail}
          handleViewAddToCart={this.props.handleAddToCart}
        />
      );
    });
  };
  render() {
    return <div className="row m-auto">{this.renderListShoe()}</div>;
  }
}
