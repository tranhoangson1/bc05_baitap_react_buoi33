import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 p-2 ">
        <div className="card text-left ">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <button
              className="btn btn-success mr-5"
              onClick={() => {
                this.props.handleViewAddToCart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
